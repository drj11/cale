package main

import (
	"archive/zip"
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"os"
)

// This is a Go program to make a Python wheel file

func main() {

	progver := "cale-0.0.1"

	out, err := os.OpenFile("out/"+progver+"-py3-none-any.whl",
		os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}

	zipper := zip.NewWriter(out)

	// Add Python Wheel metadata files to the archive.
	var literals = []struct {
		Name, Body string
	}{
		{progver + ".dist-info/WHEEL",
			"Wheel-Version: 1.0\nGenerator: " + progver + "\nRoot-Is-Purelib: false\nTag: py2-none-any\nTag: py3-none-any\nBuild: 1\n"},
	}
	for _, file := range literals {
		f, err := zipper.Create(file.Name)
		if err != nil {
			log.Fatal(err)
		}
		_, err = f.Write([]byte(file.Body))
		if err != nil {
			log.Fatal(err)
		}
	}

	// Copy files from disk.
	var files = []struct {
		Source, Target string
		Mode           os.FileMode
	}{
		{"cale", progver + ".data/scripts/cale", 0555},
		{"METADATA", progver + ".dist-info/METADATA", 0444},
	}

	record := new(bytes.Buffer)

	for _, file := range files {
		sum := CopyToZip(zipper, file.Target,
			file.Source, file.Mode)
		fmt.Fprintf(record, "%s,sha256=%s\n", file.Target, sum)
	}

	// Add the RECORD file.

	w, err := zipper.Create(progver + ".dist-info/RECORD")
	if err != nil {
		log.Fatal(err)
	}
	w.Write(record.Bytes())

	zipper.Close()

}

func CopyToZip(z *zip.Writer, target, source string, mode os.FileMode) string {

	h := &zip.FileHeader{
		Name:   target,
		Method: zip.Deflate,
	}
	h.SetMode(mode)

	out, err := z.CreateHeader(h)
	if err != nil {
		log.Fatal(err)
	}

	in, err := os.Open(source)
	if err != nil {
		log.Fatal(err)
	}
	defer in.Close()
	_, err = io.Copy(out, in)
	if err != nil {
		log.Fatal(err)
	}

	// Compute hash
	in, err = os.Open(source)
	if err != nil {
		log.Fatal(err)
	}
	defer in.Close()
	hash := sha256.New()
	_, err = io.Copy(hash, in)
	if err != nil {
		log.Fatal(err)
	}
	return hex.EncodeToString(hash.Sum(nil))
}
