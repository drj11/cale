cale: main.go
	go fmt
	go build -ldflags="-X main.version=$$(git describe --always --long --dirty)"

test: .phony
	PATH=$$PWD:$$PATH fixrun

wheel/wheel: wheel/main.go
	cd wheel && go fmt && go build

zip: .phony METADATA cale wheel/wheel
	mkdir -p out
	wheel/wheel

.phony:
