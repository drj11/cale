package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"
)

var version = "undefined"

var IsoRE = regexp.MustCompile("(\\d{4})-(\\d{1,2})")

func main() {
	var vp = flag.Bool("version", false, "report version to stdout")
	flag.Parse()

	if *vp {
		fmt.Println(version)
		return
	}

	now := time.Now()
	t := now

	// cale         # Display current month and its two neighbours
	// cale MM      # Display month MM in current year and its two neighbours
	// cale YYYY    # Display 12 months of year YYYY
	// cale YYYY-MM # Display month and its two neighbours

	if len(os.Args) <= 1 {
		display3(t, now)
		return
	}

	arg := os.Args[1]
	ym := IsoRE.FindStringSubmatch(arg)
	switch {
	case len(arg) <= 2:
		// assumed to be a month
		m, err := strconv.Atoi(arg)
		if err != nil {
			log.Fatal(err)
		}
		t = time.Date(t.Year(), time.Month(m), 15, 12, 0, 0, 0, t.Location())
	case len(arg) == 4:
		// assumed to be a year
		y, err := strconv.Atoi(arg)
		if err != nil {
			log.Fatal(err)
		}
		t = time.Date(y, 2, 15, 12, 0, 0, 0, t.Location())
		display12(t, now)
		return
	case ym != nil:
		y, err := strconv.Atoi(ym[1])
		if err != nil {
			log.Fatal(err)
		}
		m, err := strconv.Atoi(ym[2])
		if err != nil {
			log.Fatal(err)
		}
		t = time.Date(y, time.Month(m), 15, 12, 0, 0, 0, t.Location())
	}
	display3(t, now)
}

// Display 12 months
func display12(t, highlight time.Time) {
	display3(t, highlight)
	t = t.AddDate(0, 3, 0)
	display3(t, highlight)
	t = t.AddDate(0, 3, 0)
	display3(t, highlight)
	t = t.AddDate(0, 3, 0)
	display3(t, highlight)
}

// Display 3 consecutive months side-by-side
// with the month containing t in the middle,
// highlighting the day containing highlight.
func display3(t, highlight time.Time) {

	// time instance for 1st day of month
	first := time.Date(t.Year(), t.Month(), 1, 12, 0, 0, 0, t.Location())
	prevMonth := first.AddDate(0, -1, 0)
	nextMonth := first.AddDate(0, 1, 0)

	// slice of rendered months
	ms := [][]string{}
	for _, m := range []time.Time{prevMonth, t, nextMonth} {
		block := renderMonth(m, highlight)
		ms = append(ms, block)
	}

	// Always print 7 rows (including title) to accommodate
	// a month with 31 days starting on Friday, Saturday, or Sunday.
	for i := 0; i < 7; i += 1 {
		separator := ""
		for _, block := range ms {
			fmt.Print(separator)
			if i < len(block) {
				fmt.Print(block[i])
			} else {
				fmt.Printf("%21s", "")
			}
			separator = " "
		}
		fmt.Println("")
	}
}

// Render the month containing t, highlighting the day highlight.
func renderMonth(t, highlight time.Time) []string {
	title := fmt.Sprintf("     %.3s     %04d-%02d ", t.Month(), t.Year(), t.Month())
	rows := []string{title}

	firstOf := time.Date(t.Year(), t.Month(), 1, 12, 0, 0, 0, t.Location())

	// Start output on a Monday
	firstMonday := firstOf
	for firstMonday.Weekday() != 1 {
		firstMonday = firstMonday.AddDate(0, 0, -1)
	}

	monday := firstMonday
	for {
		row := renderWeek(monday, t.Month(), highlight)
		rows = append(rows, row)
		monday = monday.AddDate(0, 0, 7)
		if monday.Month() != t.Month() {
			break
		}
	}
	return rows
}

// render a week of days (one row);
// days outside the month m are not drawn (spaces);
// the day corresponding to highlight is highlighted.
func renderWeek(t time.Time, m time.Month, highlight time.Time) string {
	s := ""
	for {
		if t.Month() != m {
			s += "   "
		} else {
			if sameDay(t, highlight) {
				s += fmt.Sprintf("\x1B[7m%2d\x1B[m ", t.Day())
			} else {
				s += fmt.Sprintf("%2d ", t.Day())
			}
		}
		if t.Weekday() == 0 {
			return s
		}
		t = t.AddDate(0, 0, 1)
	}
}

func sameDay(p, q time.Time) bool {
	return p.Year() == q.Year() && p.YearDay() == q.YearDay()
}
